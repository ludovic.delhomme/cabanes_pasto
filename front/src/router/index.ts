import Home from '@/views/Home.vue'
import LoginForm from '@/components/auth/LoginForm.vue'
import ForgotPasswordForm from '@/components/auth/ForgotPasswordForm.vue'
import ResetPasswordForm from '@/components/auth/ResetPasswordForm.vue'
import Account from '@/views/admin/Account.vue'
import Users from '@/views/admin/Users.vue'
import Roles from '@/views/admin/Roles.vue'
import Permissions from '@/views/admin/Permissions.vue'
import Map from '@/views/Map.vue'
import Logements from '@/views/Logements.vue'
import LogementSingle from '@/views/LogementSingle.vue'
import Financeurs from '@/views/Financeurs.vue'
import Interventions from '@/views/Interventions.vue'
import type { Permission } from '@/interfaces/admin'
import { createRouter, createWebHistory, type RouteLocation} from 'vue-router'
import { useAuthStore } from '@/stores/auth'
import axios from 'axios'


const ifNotAuthenticated = (to:RouteLocation)  => {
  const auth = useAuthStore()
  if (to.name !=='Login'){
    if (!auth.isAuthenticated) return { name: 'Login' }
  }
}
const ifLogRedirect = ()  => {
  const auth = useAuthStore()
  if (auth.isAuthenticated) return { name: 'Home' }
}

const ifNotAdmin = ()  => {
  const auth = useAuthStore()
  if (!auth.user.permissions.find((p:Permission) => p.slug == 'manage_backend')) return { name: 'Home' }
}
const ifNotManageUsers = ()  => {
  const auth = useAuthStore()
  if (!auth.user.permissions.find((p:Permission) => p.slug == 'manage_users' || p.slug == 'manage_all_users')) return { name: 'Home' }
}


const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/login', name: 'Login', component: LoginForm, beforeEnter: [ifLogRedirect] },
  { path: '/password/forgot', name: 'ForgotPassword', component: ForgotPasswordForm, beforeEnter: [ifLogRedirect] },
  { path: '/password/reset/:token', name: 'ResetPassword', component: ResetPasswordForm, beforeEnter: [ifLogRedirect] },
  { path: '/account', name: 'Account', component: Account, beforeEnter: [ifNotAuthenticated] },
  { path: '/users', name: 'Users', component: Users, beforeEnter: [ifNotAuthenticated,ifNotManageUsers] },
  { path: '/roles', name: 'Roles', component: Roles, beforeEnter: [ifNotAuthenticated, ifNotAdmin] },
  { path: '/permissions', name: 'Permissions', component: Permissions, beforeEnter: [ifNotAuthenticated, ifNotAdmin] },
  { path: '/carte', name: 'Carte', component: Map },
  { path: '/logements', name: 'Logements', component: Logements, beforeEnter: [ifNotAuthenticated] },
  { path: '/logement/:id', name: 'LogementSingle', component: LogementSingle, beforeEnter: [ifNotAuthenticated] },
  { path: '/financeurs', name: 'Financeurs', component: Financeurs, beforeEnter: [ifNotAuthenticated] },
  { path: '/interventions', name: 'Interventions', component: Interventions, beforeEnter: [ifNotAuthenticated] },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach(async (to:RouteLocation) => {
  const auth = useAuthStore()
  const token = localStorage.getItem('cab-token')
  if (auth.token == '' && token && to.name !=='Login') {
    auth.token = token
    axios.defaults.headers.common['Authorization'] = `Bearer ${auth.token}`
    if(!auth.user.id){
      await auth.getUser()
    }
  }
})

export default router
