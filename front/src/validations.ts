import { defineRule } from 'vee-validate'
import { required, email, min, max, numeric, regex, mimes, ext } from '@vee-validate/rules'

defineRule("required", required)
defineRule("email", email)
defineRule("min", min)
defineRule("max", max)
defineRule("numeric", numeric)
defineRule("regex", regex)
defineRule("mimes", mimes)
defineRule("ext", ext)
defineRule('confirmed', (value, [target]:string[], ctx) => {
  if (value === ctx.form[target]) {
    return true;
  }
  return 'Les mots de passe ne sont pas identiques';
});
