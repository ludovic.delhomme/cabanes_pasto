import { defineStore } from 'pinia'
import type { Auth } from '@/interfaces/auth'
import type { Permission } from '@/interfaces/admin'
import axios from 'axios'

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    token: '',
    user: {
      id: null,
      firstname:'',
      name:'',
      permissions:[]
    },        
  }),
  getters:{
    isAuthenticated: (state) =>{
      const check = state.token === '' || !state.user.id ? false : true
      return check
    },
    manageBackend : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_backend') !== undefined ? true : false
    },
    manageUsers : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_users') !== undefined ? true : false
    },
    manageAllUsers : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_all_users') !== undefined ? true : false
    },
    manageAllLogements : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_all_logements') !== undefined ? true : false
    },
    manageLogements : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_logements') !== undefined ? true : false
    },
    manageFinanceurs : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_financeurs') !== undefined ? true : false
    },
    manageDownload : (state) => {
      return state.user.permissions.find((p:Permission) => p.slug == 'manage_download') !== undefined ? true : false
    },
  },
  actions: {
    async Login(credentials:Auth['credentials']){
      try{
        const response = await axios.post('/login', credentials)
        if (response.data.success){
          localStorage.setItem('cab-token', response.data.data.token)
          axios.defaults.headers.common['Authorization'] = `Bearer  ${response.data.data.token}`
          this.$state.token = response.data.data.token
          await this.getUser()
          return true
        }
      }
      catch(err){
        localStorage.removeItem('cab-token')
        return false
      }
    },
    async Logout(){
      try{
        const response = await axios.post('/logout')
        if (response.data.success){
          this.$state.token = ''
          this.$state.user.permissions = []
          localStorage.removeItem('cab-token')
          return true
        }
      }
      catch(err){
        localStorage.removeItem('cab-token')
        return err
      }
    },
    async getUser(){
      try{
        const response = await axios.get('/me')
        if (response){
          this.$state.user = response.data.data
        }
      }
      catch(err){
        return err
      }
    }
  }
})
