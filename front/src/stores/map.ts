import { defineStore } from 'pinia'
import { useApi } from '@/services/Api'
import type { LngLatBounds } from 'maplibre-gl'
import type { FeatureCollection } from 'geojson'
import { useAuthStore } from '@/stores/auth'

export const useMapStore = defineStore({
  id: 'map',
  state: () => ({
    selectedDep: null,
    data: null as unknown as FeatureCollection,
    bbox: null as unknown as LngLatBounds,
    selectedLayer: 'type',
    search: null as unknown as string,
    layers:[
      {id:'type', name:'Type de logement'},
      {id:'proprietaire', name:'Type de propriétaire'},
      {id:'etat_batiment', name:'Etat des bâtiments'},
      {id:'duree_utilisation', name:'Durée d\'utilisation'},
      {id:'surface', name:'Surface des logements'},
      {id:'nb_chambres', name:'Nombre de chambres'},
      {id:'dispo_eau', name:'Disponibilité de l\'eau'},
      {id:'douche', name:'Présence d\'une douche'},
      {id:'wc', name:'Présence d\'un wc'},
      {id:'mixite', name:'Mixité des locaux'},
      {id:'multiusage', name:'Usages'},
      {id:'accueil_public', name:'Accueil du public'},
      {id:'activite_laitiere', name:'Activité laitière'},
    ],
    publicLayers:[
      {id:'type', name:'Type de logement'},
      {id:'proprietaire', name:'Type de propriétaire'},
      {id:'multiusage', name:'Usages'},
      {id:'accueil_public', name:'Accueil du public'},
      {id:'activite_laitiere', name:'Activité laitière'},
    ],
    filters:{
      statut:'existant',
      type:'',
      proprietaire:'',
      surface:'',
      duree_utilisation: null as number | null,
      dep:'',
    }
  }),
  actions: {
    async getData(){
      const auth = useAuthStore()
      const { data } = await useApi<FeatureCollection>(auth.isAuthenticated ? '/logements/geojson' : '/logements/geojson-public' ,{method:'get'})
      this.$state.data = data.value as FeatureCollection
    }
  }
})