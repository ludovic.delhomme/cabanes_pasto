import axios from 'axios'
import { useAuthStore } from '@/stores/auth'

const axiosConfig = async () => {
  const BASE_URL = import.meta.env.VITE_BACK_URL || 'http://localhost'
  const auth = useAuthStore()
  axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
  axios.defaults.headers.common['Accept'] = 'application/json'
  axios.defaults.baseURL = `${BASE_URL}/api/v1/`
  axios.defaults.withCredentials = true
  axios.defaults.withXSRFToken = true
  if (auth.isAuthenticated) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${auth.token}`
  }

  axios.interceptors.response.use(
    response =>{
      return response
    }, 
    async error =>{
      const originalRequest = error.config
      if (error.response.status === 419 ) {
        originalRequest._retry = true
        await axios.get(`${BASE_URL}/sanctum/csrf-cookie`)
        axios(originalRequest)
      }
      return error
    }
  )
}

export default axiosConfig
