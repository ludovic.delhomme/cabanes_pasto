import type { Permission } from '@/interfaces/admin'
import { useApi } from '@/services/Api'
export const useSchema = async () => {
  const permissions = await useApi<Permission[]>('/permissions',{method:'get'})
  return[
    {
      label:'Slug',
      name:'slug',
      as:'text',
      rules:'required'
    },
    {
      label:'Nom',
      name:'name',
      as:'text',
      rules:'required'
    },
    {
      label:'Permissions',
      name:'permissions',
      as:'switch-multiple',
      options:permissions.data,
      rules:'required'
    },
  ]
}
