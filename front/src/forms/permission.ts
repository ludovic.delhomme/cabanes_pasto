export const schema = [
    {
      label:'Slug',
      name:'slug',
      as:'text',
      rules:'required'
    },
    {
      label:'Nom',
      name:'name',
      as:'text',
      rules:'required'
    }
  ]
  