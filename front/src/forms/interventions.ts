export const schema = [
  {
    label:'Statut',
    name:'statut',
    as:'radio',
    options:[
      {id:'intervention', name:'Intervention'},
      {id:'besoin', name:'Besoin'},
    ],
    default:'intervention',
    info:'Identification de l\'intervention en tant qu\'"intervention ayant eu lieu" (par défaut) ou comme "besoin" identifié pour des travaux futurs',
    rules:'required'
  },
  {
    label:'Logement',
    name:'logement_id',
    as:'autocomplete-logements',
    rules:'required',
    editable:false
  },
  {
    label:`Année de l'intervention`,
    name:'annee',
    as:'text',
    info:`Année de l'intervention`,
    rules:'numeric'
  },
  {
    label:'Type d\'intervention',
    name:'type',
    as:'select',
    options:[
      {id:null, name:'Inconnu'},
      {id:'construction', name:'Construction'},
      {id:'agrandissement', name:'Agrandissement'},
      {id:'rénovation', name:'Rénovation'},
      {id:'aménagement', name:'Aménagement'},
    ],
    info:`Type d'intervention (Aménagement = aménagement intérieur ou ajout d'équipements sans travaux de rénovation)`,
    expanded:false,
  },
  {
    label:`Financements`,
    name:'financements',
    as:'financements',
    info:`Mode de financement de l'intervention`,
  },
  {
    label:`Commentaire`,
    name:'commentaire',
    as:'textarea',
    info:`Commentaire libre pour identifier le besoin lorsque Statut = Besoin`,
  }, 
]