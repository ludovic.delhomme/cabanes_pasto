import type { Role } from "@/interfaces/admin"
import { useApi } from "@/services/Api"
  import { useAuthStore } from '@/stores/auth'

export const useSchema = async () => {
  const auth = useAuthStore()
  const roles = await useApi<Role[]>('/roles',{method:'get'})
  return [
    {
      label:'Prénom',
      name:'firstname',
      as:'text',
      rules:'required'
    },
    {
      label:'Nom',
      name:'name',
      as:'text',
      rules:'required'
    },
    {
      label:'Adresse mail',
      name:'email',
      as:'text',
      rules:'required|email'
    },
    {
      label:'N° de téléphone',
      name:'tel',
      as:'text'
    },
    {
      label:'Organisme',
      name:'organization',
      as:'text',
      rules:'required'
    },
    {
      label:'Rôle',
      name:'role',
      as:'select',
      options: auth.manageAllUsers ? roles.data : [roles.data.value[0]],
      rules:'required'
    },
    {
      label:'Mot de passe',
      name:'password',
      as:'password',
      rules:'required|min:5'
    },
    {
      label:'Confirmation du mot de passe',
      name:'password_confirmation',
      as:'password',
      rules:'required|min:5|confirmed:password'
    }
  ]
}