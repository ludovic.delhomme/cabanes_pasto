import type { Logement } from "./logements"

export interface Financeur {
  id?:number,
  nom: string,
}

export interface Intervention {
  id?:number,
  statut: string,
  logement?: Logement,
  annee: number,
  type: string,
  commentaire: string,
  financement: Financement
}

export interface Financement {
  intervention_id?: number,
  financeur: Financeur,
  montant: number,
}