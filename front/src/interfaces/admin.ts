export interface Role {
  id?:number,
  slug:string,
  name:string
  permissions?:Permission[]
}

export interface Permission {
  id?:number,
  slug:string,
  name:string
}

export interface User {
  id:number,
  firstname:string,
  name:string,
  email:string,
  tel:string,
  organization:string,
  role?:Role,
  permissions?:Permission[]

}