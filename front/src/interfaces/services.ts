export interface confirmDeleteConfig{
    url:string,
    confirmMsg:{
      title:string,
      message:string
    },
    successMsg:string
  }

export interface Insee{
  code:string,
  codeDepartement:string,
  nom:string
}