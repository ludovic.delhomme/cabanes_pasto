import type { User } from "./admin"
import type { Feature } from 'maplibre-gl'
import type { Intervention } from "./interventions"
export interface Logement {
  id?:number,
  statut:string,
  departement:string,
  commune:string,
  up_zp?:string,
  nom?:string,
  acces_final?:string,
  temps_acces?:number,
  proprietaire?:string,
  type?:string,
  multiusage:string,
  accueil_public: string,
  activite_laitiere?:string,
  duree_utilisation?:number,
  etat_batiment?:string,
  couchages_non_salaries?:number,
  salaries_max?:number,
  mixite?:boolean,
  nb_chambres?:number,
  surface?:string,
  douche?:string,
  wc?:string,
  alim_elec?:string,
  alim_eau?:string,
  origine_eau?:string,
  qualite_eau?:string,
  dispo_eau?:string,
  assainissement?:string,
  chauffe_eau?:string,
  chauffage?:boolean,
  stockage_indep?:boolean,
  user:User,
  date_modif:string,
  geom:Feature,
  images:Image[],
  interventions:Intervention[],
}

export interface Simulation {
  id?:number,
  name:string,
  comment:string,
  manage_by:User,
  result:Result,
  ref_cog:string,
  visible_by:User[],
  file:File,
  pdf:File,
  send_at?:string,
  geom:Feature,
  created_at:string,
}

export interface Perimeter {
  id:string,
  name:string,
  year:string,
  the_geom?:Feature, 
}

export interface Result {
  altitude: Classe[],
  pente:Classe[],
  Alti_moy_MNT: number,
  Alti_moy_hpa: number,
  Handi_Alti: number,
  Pente_moy_MNT: number,
  Pente_moy_hpa: number,
  Handi_Pente: number,
  Prop_nodata: number,
  In_Vosges: number
}

export interface Classe {
  classe: string,
  surface: number,
  prop: number 
}

export interface File {
  id:number,
  folder:string,
  type:string,
  name:string,
}

export interface Image {
  id: number,
  logement_id: string,
  name: string,
}