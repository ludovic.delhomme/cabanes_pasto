import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import Oruga from '@oruga-ui/oruga-next'
import { bulmaConfig } from '@oruga-ui/theme-bulma'
import router from './router'
import './validations'
import axiosConfig from './axios'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)
dom.watch()

const app = createApp(App)
app.use(router)
app.use(createPinia())
app.use(Oruga,{
    ...bulmaConfig,
    iconPack: 'fas'
  }  
)
axiosConfig()
app.mount('#app')
app.config.globalProperties.$oruga = Oruga

