import { useProgrammatic } from '@oruga-ui/oruga-next'
import { AxiosError } from 'axios'

const { oruga } = useProgrammatic()

export function useNotification(config:any){
  oruga.notification.open(config)
}

export function useErrorApiNotification(error:unknown){
  if (error instanceof AxiosError){
    useNotification({
      message: error.response?.data.message,
      position: 'top',
      variant: 'danger',
      duration:10000
    })
  } else {
    useNotification({
      message: error,
      position: 'top',
      variant: 'danger',
      duration:10000
    })
  }
}


