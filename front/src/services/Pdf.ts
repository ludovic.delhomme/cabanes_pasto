import jsPDF from 'jspdf'
import { regular, bold } from '@/assets/font/Roboto'
import autoTable, { type RowInput } from 'jspdf-autotable'
import type { Perimeter, Simulation } from '@/interfaces/logements'
import { useApi } from './Api'
import { Map, type MapGeoJSONFeature } from 'maplibre-gl'
import bbox from '@turf/bbox'

function getBbox(data:MapGeoJSONFeature){
  const bounds = bbox(data)
  const coords:[number,number,number,number] = [bounds[0],bounds[1],bounds[2],bounds[3]]
  return coords
}

export async function mapPDF(geom:any) {
  let result = ''
  const map = new Map({
    container: 'map',
    style: 'https://openmaptiles.geo.data.gouv.fr/styles/osm-bright/style.json',
    center: [4.5, 45.6],
    zoom: 5,
    attributionControl: false,
    preserveDrawingBuffer: true,
    interactive: false,
  })
  const dpi = 300;
  Object.defineProperty(window, 'devicePixelRatio', {
      get: function() {return dpi / 96}
  })
  map.on('style.load', () => {
    map.addSource('zone', {
      type: 'geojson',
      data: geom
    })
    map.addSource('geoportail', {
      type: 'raster',
      tiles: [
        "https://wxs.ign.fr/agriculture/geoportail/wmts?" +
        "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" +
        "&STYLE=normal" +
        "&TILEMATRIXSET=PM" +
        "&FORMAT=image/png"+
        "&LAYER=LANDUSE.AGRICULTURE.LATEST"+
        "&TILEMATRIX={z}" +
        "&TILEROW={y}" +
        "&TILECOL={x}"
      ],
      'tileSize': 256,
      'attribution' : '&copy; <a href="http://www.ign.fr/">IGN</a>'
    })
    map.addLayer({
      id: 'geoportail',
      source: 'geoportail',
      type: 'raster',
      minzoom: 10,
      paint: {
        'raster-opacity':0.4,
      },
    })
    map.addLayer({
      id: 'zoneLayer',
      type: 'fill',
      source: 'zone',
      paint: {
        'fill-color':'#e34a33',
        'fill-opacity':0.7,
      },
    })
  })
  map.on('load',() => {
    map.fitBounds(getBbox(geom as MapGeoJSONFeature), {padding: 20})
    result = map.getCanvas().toDataURL()
    console.log(result)
  })
  return result
}

export async function createPDF(data:Simulation) {
  const pdf = new jsPDF({orientation: 'portrait', unit:'mm'})
  const pageWidth = pdf.internal.pageSize.width || pdf.internal.pageSize.getWidth()
  const wantedTableWidth = 100
  const marginTable = (pageWidth - wantedTableWidth) / 2
  const columns = [
    {
      field: 'classe',
      label: 'Classe',
      width: '30'
    },
    {
      field: 'surface',
      label: 'Surface (ha)',
      width: '30'
    },
    {
      field: 'prop',
      label: 'Proportion (%)',
      width: '30'
    }
  ]
  const sum_surface = data.result.pente.reduce((a:number, b:any) => {
    return a + b.surface
  }, 0)
  const perimeter = (await useApi<Perimeter[]>('/perimeters/search',{method:'post',data:{'text':data.ref_cog}})).data.value
  const map = await mapPDF(data.geom)
  pdf.addFileToVFS('Roboto-normal.ttf', regular)
  pdf.addFont('Roboto-normal.ttf', 'Roboto', 'normal')
  pdf.addFileToVFS('Roboto-bold.ttf', bold)
  pdf.addFont('Roboto-bold.ttf', 'Roboto', 'bold')
  pdf.setFont('Roboto','bold')
  let startY = 15
  // Logo INRAE
  const img = new Image()
  img.src = '/inrae.png'
  pdf.addImage(img, "PNG", 152, 10, 47.26,12.5)
  const mapImg = new Image()
  mapImg.src = map
  //pdf.addImage(mapImg, "PNG", 152, 10, 47.26,12.5)
  // Titre du pdf : Résultat du calcul pente altitude (provenant du template)
  pdf.setFontSize(15)
  pdf.text( "Résultats du calcul Pente-Altitude", 15, startY)
  // Commune
  pdf.setFontSize(9)
  pdf.setFont('Roboto','normal')
  pdf.text( `Commune concernée: ${perimeter[0].name} (${data.ref_cog})`, 15, startY+=5)
  // département
  pdf.text( `Département: ${data.ref_cog.substring(0,2)}`, 15, startY+=5)
  // Date  
  pdf.setFontSize(9)
  pdf.text( `Calcul réalisé le ${new Date().toLocaleDateString('fr-FR')}`, 15, startY+=5)
  // Source
  pdf.text( `D'après le fichier ${data.file.name}`, 15, startY+=5)
  pdf.text( `Surface: ${sum_surface} ha`, 15, startY+=5)
  pdf.text( `Barême Vosges: ${data.result.In_Vosges == 1 ? 'Oui' : 'Non'}`, 15, startY+=5)
  //Tableau pente
  pdf.setFontSize(12)
  pdf.setFont('Roboto','bold')
  pdf.text( `Répartition en classes de pentes`, marginTable, startY+=10)
  const colnames = columns.map(function(e){
    return `${e.label}`;
  })
  const pentedatas:RowInput[] = []
  data.result.pente.forEach((e:any) => {
    pentedatas.push([e.classe, e.surface.toLocaleString(), e.prop.toLocaleString()])
  })
  //généation de la table
  autoTable(pdf,
    {
      head: [colnames],
      body: pentedatas,
      startY: startY + 3,
      theme: 'grid',
      margin: {top: 0, right: marginTable, left: marginTable, bottom: 0 },
      styles:{
        font:'Roboto',
        fontSize: 10,
        cellPadding: 1
      }
    }
  )
  startY = (pdf as any).previousAutoTable.finalY + 10 //this gives you the value of the end-y-axis-position of the previous autotable.
  //Tableau altitudes
  pdf.text( `Répartition en classes d'altitudes`, marginTable, startY)
  const altidatas:RowInput[] = []
  data.result.altitude.forEach((e:any) => {
    altidatas.push([e.classe, e.surface.toLocaleString(), e.prop.toLocaleString()])
  })
  //généation de la table
  autoTable(pdf,
    {
      head: [colnames],
      body: altidatas,
      startY: startY + 3,
      theme: 'grid',
      margin: {top: 0, right: marginTable, left: marginTable, bottom: 0 },
      tableWidth:100,
      styles:{
        font:'Roboto',
        fontSize: 10,
        cellPadding: 1
      }
    }
  )
  startY = (pdf as any).previousAutoTable.finalY + 15; //this gives you the value of the end-y-axis-position of the previous autotable.
  // Synthèse
  pdf.rect(43, startY-10, 130, 40, 'S');
  pdf.setFont('Roboto','normal')
  pdf.text( `Pente Moyenne: ${data.result.Pente_moy_hpa} %`, 45, startY)
  pdf.text( `Altitude Moyenne: ${data.result.Alti_moy_hpa} m`, 120, startY)
  pdf.text( `Handicap Pente: ${data.result.Handi_Pente}`, 45, startY+=5)
  pdf.text( `Handicap Altitude: ${data.result.Handi_Alti}`, 120, startY)
  pdf.setFont('Roboto', 'bold')
  pdf.text( `Handicap Pente/Altitude: ${(data.result.Handi_Pente + data.result.Handi_Alti).toFixed(2)}`, marginTable+22, startY+=15)
  //Footer
  pdf.setFont('Roboto', 'normal')
  pdf.setFontSize(8)
  pdf.text( `UR LESSEM, 2 rue de la Papeterie - BP76 - 38402 Saint Martin d'Hères`, 15, 280)
  pdf.text( `Tel : 04 76 76 27 74`, 15, 285)
  return pdf
}