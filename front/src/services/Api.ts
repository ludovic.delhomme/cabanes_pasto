import { ref } from 'vue'
import axios, { type AxiosError, type AxiosRequestConfig, type AxiosResponse } from 'axios'

export const useApi = async <T>(url:string, config:AxiosRequestConfig) =>{
  const loading = ref(false)
  const response= ref<AxiosResponse | null>(null)
  const data = ref<T | []>([])
  const error = ref<AxiosError | null>(null)
  const fetch = async () =>{
    loading.value = true
    try{
      const result = await axios.request({url,...config})
      response.value = result
      data.value = result.data.data
      loading.value = false
    } catch (err){
      if(axios.isAxiosError(err)){
        error.value = err
      }
    } finally {
      loading.value = false
    }
  }
  await fetch()
  return { loading, response, data, error, fetch }
}

export const usePaginateApi = async <T>(url:string, config:AxiosRequestConfig) =>{
  const loading = ref(false)
  const response= ref<AxiosResponse[] | null>(null)
  const data = ref<T[] | []>([])
  const error = ref<AxiosError | null>(null)
  const endpoints:string[] = []
  const fetch = async () =>{
    loading.value = true
    try{
      const firstEndpoint = await axios.request({url,...config})
      const endpointsNumber = Number(firstEndpoint.data.meta.last_page)
      for (let i = 2; i <= endpointsNumber; i++) {
        endpoints.push(`${url}?page=${i}`)
      }
      response.value = await Promise.all(endpoints.map((endpoint)=>axios.request({url:endpoint,...config})))
      response.value?.push(firstEndpoint)
      data.value = response.value.map((res:AxiosResponse) =>{
        return res.data.data
      }).flat()
      loading.value = false
    } catch (err){
      if(axios.isAxiosError(err)){
        error.value = err
      }
    } finally {
      loading.value = false
    }
  }
  await fetch()
  return { loading, response, data, error, fetch }
}

