import { useProgrammatic } from '@oruga-ui/oruga-next'
import { useApi, usePaginateApi } from './Api'
import type { confirmDeleteConfig }  from '@/interfaces/services'
import type { Component } from 'vue'
import { useErrorApiNotification, useNotification } from './Notifications'

const { oruga } = useProgrammatic()


export function useModal(config:any):any{
  oruga.modal.open(config)
}

export async function useConfirmDeleteModal<T>(container:T[], component:Component, row:T | any, config:confirmDeleteConfig) {
  const instance = oruga.modal.open({
    component: component,
    props: config.confirmMsg,
    trapFocus: true,
  })
  const result = await instance.promise
  if(result.action == 'confirm'){
    const { response } = await useApi(`/${config.url}/${row.id}`,{method:'delete'})
    if (response.value?.status === 200){
      container.splice(container.indexOf(row as never), 1)
      useNotification({
        message: config.successMsg,
        position: 'top',
        variant: 'success',
      })
    }
    else{
      useErrorApiNotification(response.value)
    }
  } 
}

export function useModalWithRefresh<T>(component:Component, props:any,url:string,container:T[]){
  useModal({
    component: component,
    props:props,
    canCancel:['button'],
    trapFocus: true,
    events:{
      'refresh': async () =>{
        const { data } = await usePaginateApi(`/${url}`,{method:'get'})
        if (data){
          container.length = 0
          container.push(...data.value as T[])
        }
      }
    }
  })
}