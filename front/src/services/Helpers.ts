export const slugify = (...args: (string | number)[]): string => {
  const value = args.join(' ')

  return value
    .normalize('NFD') // split an accented letter in the base letter and the acent
    .replace(/[\u0300-\u036f]/g, '') // remove all previously split accents
    .toLowerCase()
    .trim()
    .replace(/[^a-z0-9 ]/g, '') // remove all chars not letters, numbers and spaces (to be replaced)
    .replace(/\s+/g, '-') // separator
}

export const getTooltip = (schema:any[], field: string) => {
 const response = schema.find(s => s.name === field )
 return response ? response.info : null
}

export const booleanWithNull = (data: boolean | null) => {
  const text = typeof data == 'boolean' ? `${data === true ? 'Oui' : 'Non'}` : 'Non renseigné'
  return text
}

export const numberWithNull = (data: number | null) => {
  const text = typeof data == 'number' ? data : 'Non renseigné'
  return text
}