import type { GeoJSONFeature, MapGeoJSONFeature } from 'maplibre-gl'
import bbox from '@turf/bbox'
import buffer from '@turf/buffer'

export function getBbox(data:MapGeoJSONFeature){
  const bounds = bbox(data)
  const coords:[number,number,number,number] = [bounds[0],bounds[1],bounds[2],bounds[3]]
  return coords
}

export function getBboxFromPoint(data:GeoJSONFeature){
  const buffered = buffer(data, 1, {units:'kilometers'})
  const bounds = bbox(buffered)
  const coords:[number,number,number,number] = [bounds[0],bounds[1],bounds[2],bounds[3]]
  return coords
}