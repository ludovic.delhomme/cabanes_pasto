FROM php:8.2-apache

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y \
        curl \
        libzip-dev \
        libpq-dev \
        libpng-dev \
        unzip \
        && docker-php-ext-install gd zip pgsql pdo_pgsql

COPY ./docker/prod/api/vhost.conf /etc/apache2/sites-available/000-default.conf

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer