<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if(! app()->runningInConsole() ) {
            if (Schema::hasTable('roles')) {
                $roles = Role::with('permissions')->get();
                $permissionsArray = [];
                foreach ($roles as $role) {
                    foreach ($role->permissions as $permission) {
                        $permissionsArray[$permission->slug][] = $role->id;
                    }
                }
                // Every permission may have multiple roles assigned
                foreach ($permissionsArray as $slug => $roles) {
                    Gate::define($slug, function ($user) use ($roles) {
                        // We check if we have the needed roles among current user's roles
                        return count(array_intersect($user->role->pluck('id')->toArray(), $roles)) > 0;
                    });
                }
            }
        }
    }
}
