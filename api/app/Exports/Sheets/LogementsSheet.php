<?php 
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Models\Logement;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class LogementsSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize, WithStrictNullComparison, WithColumnFormatting
{
   
  public function query()
  {
    return  Logement::query();    
  }

  public function title(): string
  {
      return 'Logements pastoraux';
  }

  function longitude($geom): string
  {
    $long = DB::select("select ST_x('".$geom."') as geom;");
    return $long[0]->geom;
  }
  
  function latitude($geom): string
  {
    $lat = DB::select("select ST_y('".$geom."') as geom;");
    return $lat[0]->geom;
  }

  public function map($data): array
  {
    $map = [
      $data->id,
      $data->statut,
      $data->departement,
      $data->commune,
      $data->up_zp,
      $data->nom,
      $data->acces_final, 
      $data->temps_acces,
      $data->proprietaire,
      $data->type,
      $data->multiusage,
      $data->accueil_public,
      $data->activite_laitiere,
      $data->duree_utilisation,
      $data->etat_batiment,
      $data->couchages_non_salaries,
      $data->salaries_max, 
      $data->mixite,
      $data->nb_chambres,
      $data->surface,
      $data->douche,
      $data->wc,
      $data->alim_elec,
      $data->alim_eau,
      $data->origine_eau,
      $data->qualite_eau,
      $data->dispo_eau,
      $data->assainissement,
      $data->chauffe_eau,
      $data->chauffage,
      $data->stockage_indep,
      $this->longitude($data->geom),
      $this->latitude($data->geom),
      Date::dateTimeToExcel($data->created_at),
      $data->user_id,
    ];
    return $map;
  }

  public function headings(): array
  {
    $headings = [
      'id',
      'statut',
      'departement',
      'commune',
      'up_zp',
      'nom',
      'acces_final', 
      'temps_acces',
      'proprietaire',
      'type',
      'multiusage',
      'accueil_public',
      'activite_laitiere',
      'duree_utilisation',
      'etat_batiment',
      'couchages_non_salaries',
      'salaries_max', 
      'mixite',
      'nb_chambres',
      'surface',
      'douche',
      'wc',
      'alim_elec',
      'alim_eau',
      'origine_eau',
      'qualite_eau',
      'dispo_eau',
      'assainissement',
      'chauffe_eau',
      'chauffage',
      'stockage_indep',
      'x',
      'y',
      'created_at',
      'user_id',
    ]; 
    return $headings;
  }

  public function columnFormats(): array
  {
    return [
      'AH' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    ];
  }
}

