<?php 
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Intervention;

class InterventionsSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{
   
  public function query()
  {
    return  Intervention::query();    
  }

  public function title(): string
    {
        return 'Interventions';
    }

  public function map($data): array
  {
    $map = [
      $data->id,
      $data->statut,
      $data->logement_id,
      $data->annee,
      $data->type,
      $data->commentaire
    ];
    
    return $map;
  }

  public function headings(): array
  {
    $headings = [
      'id',
      'statut',
      'logement_id',
      'annee',
      'type',
      'commentaire'
    ]; 
    return $headings;
  }
}

