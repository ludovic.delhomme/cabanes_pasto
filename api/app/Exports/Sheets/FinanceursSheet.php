<?php 
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Financeur;

class FinanceursSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{
   
  public function query()
  {
    return  Financeur::query();    
  }

  public function title(): string
    {
        return 'Financeurs';
    }

  public function map($data): array
  {
    $map = [
      $data->id,
      $data->nom
    ];
    
    return $map;
  }

  public function headings(): array
  {
    $headings = [
      'id',
      'nom',
    ]; 
    return $headings;
  }
}

