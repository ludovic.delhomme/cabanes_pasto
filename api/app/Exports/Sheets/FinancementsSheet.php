<?php 
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Financement;

class FinancementsSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{
   
  public function query()
  {
    return  Financement::query();    
  }

  public function title(): string
    {
        return 'Financements';
    }

  public function map($data): array
  {
    $map = [
      $data->id,
      $data->intervention_id,
      $data->financeur_id,
      $data->montant      
    ];
    
    return $map;
  }

  public function headings(): array
  {
    $headings = [
      'id',
      'intervention_id',
      'financeur_id',
      'montant',
    ]; 
    return $headings;
  }
}

