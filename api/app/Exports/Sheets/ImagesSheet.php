<?php 
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Image;

class ImagesSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{
   
  public function query()
  {
    return  Image::query();    
  }

  public function title(): string
    {
        return 'Images';
    }

  public function map($data): array
  {
    $map = [
      $data->id,
      $data->logement_id,
      $data->name,
    ];
    
    return $map;
  }

  public function headings(): array
  {
    $headings = [
      'id',
      'logement_id',
      'name',
    ]; 
    return $headings;
  }
}

