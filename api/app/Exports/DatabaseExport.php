<?php 
namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Exports\Sheets\LogementsSheet;
use App\Exports\Sheets\ImagesSheet;
use App\Exports\Sheets\InterventionsSheet;
use App\Exports\Sheets\FinancementsSheet;
use App\Exports\Sheets\FinanceursSheet;
use App\Exports\Sheets\UsersSheet;

class DatabaseExport implements WithMultipleSheets
{
  use Exportable;
    
  public function sheets(): array
    {
      $sheets = [
        new LogementsSheet(),
        new ImagesSheet(),
        new InterventionsSheet(),
        new FinancementsSheet(),
        new FinanceursSheet(),
        new UsersSheet(),
      ];
      return $sheets;
    }
}