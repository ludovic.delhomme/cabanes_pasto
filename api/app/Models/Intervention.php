<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Intervention extends Model
{
    protected $table = 'interventions';

    protected $fillable = [
      'statut',
      'logement_id',
      'annee',
      'type',
      'commentaire',
    ];
    
    public function logement()
    {
      return $this->belongsTo(Logement::class);
    }

    public function financements()
    {
      return $this->hasMany(Financement::class);
    }

}
