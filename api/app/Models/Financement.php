<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Financement extends Model
{
    protected $table = 'financements';
    protected $fillable = [
      'intervention_id',
      'financeur_id',
      'montant',
    ];
    public $timestamps = false;
    
    public function intervention()
    {
      return $this->belongsTo(Intervention::class);
    }

    public function financeur()
    {
      return $this->belongsTo(Financeur::class);
    }
    

}
