<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
      'logement_id',  
      'name',
      'created_at',
      'updated_at'
    ];
    
    public function logement()
    {
      return $this->belongsTo(Logement::class);
    }
}