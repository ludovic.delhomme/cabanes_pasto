<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Financeur extends Model
{
    protected $table = 'financeurs';

    protected $fillable = [
      'nom',
    ];
    
    public function financements()
    {
      return $this->hasMany(Financement::class);
    }
}
