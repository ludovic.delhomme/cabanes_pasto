<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logement extends Model
{
    protected $table = 'logements';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
      'id',
      'statut',
      'departement',
      'commune',
      'up_zp',
      'nom',
      'acces_final',
      'temps_acces',
      'proprietaire',
      'type',
      'multiusage',
      'accueil_public',
      'activite_laitiere',
      'duree_utilisation',
      'etat_batiment',
      'couchages_non_salaries',
      'salaries_max',
      'mixite',
      'nb_chambres',
      'surface',
      'douche',
      'wc',
      'alim_elec',
      'alim_eau',
      'origine_eau',
      'qualite_eau',
      'dispo_eau',
      'assainissement',
      'chauffe_eau',
      'chauffage',
      'stockage_indep',
      'user_id',
      'geom',
      'created_at',
      'updated_at'
    ];
    
    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function interventions()
    {
      return $this->hasMany(Intervention::class);
    }

    public function images()
    {
      return $this->hasMany(Image::class);
    }

}
