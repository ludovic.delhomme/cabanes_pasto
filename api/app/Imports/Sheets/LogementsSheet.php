<?php 

namespace App\Imports\Sheets;

use App\Models\Logement;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Illuminate\Support\Carbon;

class LogementsSheet implements ToCollection, WithHeadingRow, WithUpserts
{
  public function uniqueBy()
  {
    return 'id';
  }
  
  public function collection(Collection $rows)
  {
    foreach ($rows as $row) 
    {
      $data = Logement::firstOrNew(['id' => $row['id']]);
      $data->id = $row['id'];
      $data->statut = $row['statut'];
      $data->departement = $row['departement'];
      $data->commune = $row['commune'];
      $data->up_zp = $row['up_zp'];
      $data->nom = $row['nom'];
      $data->acces_final = $row['acces_final']; 
      $data->temps_acces = $row['temps_acces'];
      $data->proprietaire = $row['proprietaire'];
      $data->type = $row['type'];
      $data->multiusage = $row['multiusage'];
      $data->accueil_public = $row['accueil_public'];
      $data->activite_laitiere = $row['activite_laitiere'];
      $data->duree_utilisation = $row['duree_utilisation'];
      $data->etat_batiment = $row['etat_batiment'];
      $data->couchages_non_salaries = $row['couchages_non_salaries'];
      $data->salaries_max = $row['salaries_max']; 
      $data->mixite = $row['mixite'];
      $data->nb_chambres = $row['nb_chambres'];
      $data->surface = $row['surface'];
      $data->douche = $row['douche'];
      $data->wc = $row['wc'];
      $data->alim_elec = $row['alim_elec'];
      $data->alim_eau = $row['alim_eau'];
      $data->origine_eau = $row['origine_eau'];
      $data->qualite_eau = $row['qualite_eau'];
      $data->dispo_eau = $row['dispo_eau'];
      $data->assainissement = $row['assainissement'];
      $data->chauffe_eau = $row['chauffe_eau'];
      $data->chauffage = $row['chauffage'];
      $data->stockage_indep = $row['stockage_indep'];
      if($row['x'] && $row['y']){
        $geom = DB::select("select ST_Point(".$row['x'].",".$row['y'].",4326) as geom;");
        $data->geom = $geom[0]->geom;
      }
      $data->created_at = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['created_at']));
      $data->user_id = $row['user_id'];
      $data->save();
    }
  }
}
