<?php 

namespace App\Imports\Sheets;

use App\Models\Financement;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class FinancementsSheet implements ToCollection, WithHeadingRow, WithUpserts
{
  public function uniqueBy()
  {
    return 'id';
  }
  
  public function collection(Collection $rows)
  {
    foreach ($rows as $row) 
    {
      $data = Financement::firstOrNew(['id' => $row['id']]);
      $data->id = $row['id'];
      $data->intervention_id = $row['intervention_id'];
      $data->financeur_id = $row['financeur_id'];
      $data->montant = $row['montant'];
      $data->save();
    }
  }
}
