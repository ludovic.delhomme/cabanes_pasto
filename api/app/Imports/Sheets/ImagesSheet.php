<?php 

namespace App\Imports\Sheets;

use App\Models\Image;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ImagesSheet implements ToCollection, WithHeadingRow, WithUpserts
{
  public function uniqueBy()
  {
    return 'id';
  }
  
  public function collection(Collection $rows)
  {
    foreach ($rows as $row) 
    {
      $data = Image::firstOrNew(['id' => $row['id']]);
      $data->id = $row['id'];
      $data->logement_id = $row['logement_id'];
      $data->name = $row['name'];
      $data->save();
    }
  }
}
