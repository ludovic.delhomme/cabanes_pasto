<?php 

namespace App\Imports\Sheets;

use App\Models\Financeur;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class FinanceursSheet implements ToCollection, WithHeadingRow, WithUpserts
{
  public function uniqueBy()
  {
    return 'id';
  }
  
  public function collection(Collection $rows)
  {
    foreach ($rows as $row) 
    {
      $data = Financeur::firstOrNew(['id' => $row['id']]);
      $data->id = $row['id'];
      $data->nom = $row['nom'];
      $data->save();
    }
  }
}
