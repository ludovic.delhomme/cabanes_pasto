<?php 

namespace App\Imports\Sheets;

use App\Models\Intervention;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class InterventionsSheet implements ToCollection, WithHeadingRow, WithUpserts
{
  public function uniqueBy()
  {
    return 'id';
  }
  
  public function collection(Collection $rows)
  {
    foreach ($rows as $row) 
    {
      $data = Intervention::firstOrNew(['id' => $row['id']]);
      $data->id = $row['id'];
      $data->statut = $row['statut'];
      $data->logement_id = $row['logement_id'];
      $data->annee = $row['annee'];
      $data->type = $row['type'];
      $data->commentaire = $row['commentaire'];
      $data->save();
    }
  }
}
