<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Imports\Sheets\LogementsSheet;
use App\Imports\Sheets\ImagesSheet;
use App\Imports\Sheets\InterventionsSheet;
use App\Imports\Sheets\FinancementsSheet;
use App\Imports\Sheets\FinanceursSheet;


class DatabaseImport implements WithMultipleSheets
{
  public function sheets(): array
  {
    return [
      'Financeurs' => new FinanceursSheet(),
      'Logements pastoraux' => new LogementsSheet(),
      'Images' => new ImagesSheet(),
      'Interventions' => new InterventionsSheet(),
      'Financements' => new FinancementsSheet(),
    ];
  }
}
