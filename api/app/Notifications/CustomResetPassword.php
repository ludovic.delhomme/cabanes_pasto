<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword;

class CustomResetPassword extends ResetPassword
{
    
    protected function buildMailMessage($url)
    {
        return (new MailMessage)
            ->subject('Renouveler votre mot de passe')
            ->greeting('Bonjour,')
            ->line('Vous recevez cet e-mail car nous avons reçu une demande de réinitialisation du mot de passe pour votre compte.')
            ->line('Ce lien de réinitialisation de mot de passe expirera dans 60 minutes.')
            ->line("Si vous n'êtes pas à l'origine de cette demande, merci de contacter l'administrateur de cette application.")
            ->action('Changer votre mot de passe', $url)
            ->salutation("Cordialement, l'équipe Cabanes pastorales");
    }
}
