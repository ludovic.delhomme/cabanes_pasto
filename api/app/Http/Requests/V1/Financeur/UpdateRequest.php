<?php
namespace App\Http\Requests\V1\Financeur;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$id = $this->financeur;
    	return $rules = [
			'nom' => 'required|max:255,nom,'.$id,
		];
	}
}
