<?php
namespace App\Http\Requests\V1\Intervention;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'statut' => 'required|string|max:255',
            'logement_id' =>'required|string|max:6',
            'annee' =>'required|integer|digits:4',
            'type' =>'required|string|max:255',
            'commentaire' =>'required|string',
        ];
	}
}
