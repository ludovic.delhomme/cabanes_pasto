<?php
namespace App\Http\Requests\V1\Intervention;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$id = $this->intervention;
    	return $rules = [
			'statut' => 'required|string|max:255,statut,'.$id,
            'logement_id' => 'required|string|max:6,logement_id,'.$id,
            'annee' => 'required|integer|digits:4,annee,'.$id,
            'type' => 'required|string|max:255,type,'.$id,
            'commentaire' => 'required|string'
		];
	}
}
