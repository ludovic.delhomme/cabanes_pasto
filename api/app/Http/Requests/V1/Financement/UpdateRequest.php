<?php
namespace App\Http\Requests\V1\Financement;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$id = $this->financement;
    	return $rules = [
			'intervention_id' => 'required|integer,intervention_id,'.$id,
            'financeur_id' => 'required|integer,financeur_id,'.$id,
            'montant' =>'required|integer,montant,'.$id,
		];
	}
}
