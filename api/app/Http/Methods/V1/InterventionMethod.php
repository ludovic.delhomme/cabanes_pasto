<?php 
namespace App\Http\Methods\V1;

use App\Http\Methods\BaseMethod;
use App\Models\Intervention;
use Illuminate\Support\Facades\Auth;

class InterventionMethod extends BaseMethod
{
	public function __construct(Intervention $intervention)
	{
		$this->model = $intervention;
	}

	public function index()
	{
		return $this->model->with(['logement','financements'])->paginate(50);
	}

	public function onlyMine()
	{
		return $this->model->with(['logement','financements'])->whereHas('logement', function ($query) {
			return $query->where('user_id', Auth::user()->id);
		})->paginate(50);
	}

	public function find($id)
	{
		return $this->model->with(['logement','financements'])->findOrFail($id);
	}

	public function store($inputs)
	{
    $intervention = new $this->model;
    $intervention->fill($inputs);
    $intervention->save();
	return $intervention->id;
	}

	public function update($inputs, $id)
	{
		$intervention = $this->model->find($id);
		$intervention->fill($inputs);
		$intervention->save();
	}
    
	public function destroy($id)
	{
		$intervention = $this->model->find($id);
		$intervention->delete();
	}
}
