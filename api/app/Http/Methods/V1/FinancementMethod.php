<?php 
namespace App\Http\Methods\V1;

use App\Http\Methods\BaseMethod;
use App\Models\Financement;

class FinancementMethod extends BaseMethod
{
	public function __construct(Financement $financement)
	{
		$this->model = $financement;
	}

	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $financement = new $this->model;
	    $financement->fill($inputs);
	    $financement->save();
	}

	public function update($inputs, $id)
	{
		$financement = $this->model->find($id);
		$financement->fill($inputs);
		$financement->save();
	}
    
	public function destroy($id)
	{
		$financement = $this->model->find($id);
		$financement->delete();
	}
}
