<?php 
namespace App\Http\Methods\V1;

use App\Http\Methods\BaseMethod;
use App\Models\Logement;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LogementMethod extends BaseMethod
{
	public function __construct(Logement $logement)
	{
		$this->model = $logement;
	}

	public function index()
	{
		return $this->model->with(['images','interventions'])->select(['*', DB::raw('ST_AsGeoJSON(geom,6) as geom')])->paginate(50);
	}

	public function public()
	{
		return $this->model->select(['id','nom','accueil_public','updated_at', DB::raw('ST_AsGeoJSON(geom,6) as geom')])->paginate(200);
	}

	public function geojson()
	{
		$query = "SELECT json_build_object('type', 'FeatureCollection','features', json_agg(ST_AsGeoJSON(t.*)::json)) as geojson
		FROM (SELECT * FROM public.logements) as t";
		$data = DB::select($query);
		return json_decode($data[0]->geojson);
	}

	public function geojson_public()
	{
		$query = "SELECT json_build_object('type', 'FeatureCollection','features', json_agg(ST_AsGeoJSON(t.*)::json)) as geojson
		FROM (SELECT id, statut, departement, commune, up_zp, nom, type, proprietaire, duree_utilisation, accueil_public, multiusage, activite_laitiere, geom FROM public.logements) as t";
		$data = DB::select($query);
		return json_decode($data[0]->geojson);
	}

	public function onlyMine()
	{
		return $this->model->with(['images','interventions'])->where('user_id',Auth::user()->id)->select(['*', DB::raw('ST_AsGeoJSON(geom,6) as geom')])->paginate(50);
	}

	public function find($id)
	{
		return $this->model->with(['images','interventions','interventions.financements'])->findOrFail($id,['*', DB::raw('ST_AsGeoJSON(geom,6) as geom')]);
	}

	public function store($inputs)
	{
	    $logement = new $this->model;
		$logement->fill($inputs);
	    $logement->save();
	}

	public function update($inputs, $id)
	{
		$logement = $this->model->find($id);
		$logement->fill($inputs);
		$logement->save();
	}
    
	public function destroy($id)
	{
		$logement = $this->model->find($id);
		$logement->delete();
	}

	public function geojson_to_geom($geojson)
	{
		$query = "select ST_GeomFromGeoJSON('".json_encode($geojson)."') as geom";
		$geom = DB::select($query);
		return $geom[0]->geom;
	}

	public function get_id($dep)
	{
		$last = $this->model->where('departement',$dep)->max('id');
		$count = intval(substr($last,-4));
		$id = ''.$dep.''. sprintf('%04d', $count+1).'';
		return $id;
	}

	public function search($text, $auth)
	{
		$search = $this->model->select(array('id','nom'))
			->where('nom','ILIKE','%'.$text.'%')
			->where('id','ILIKE','%'.$text.'%','or');

		switch ($auth) {
			case 'me':
				$search->where('user_id', Auth::user()->id);
				break;
			default:
				break;
		}
	
		return $search->orderBy('id','asc')->paginate(100);
	}

	public function last()
	{
		$last = $this->model->max('updated_at');
		return ['last_update' => Carbon::parse($last)->tz('Europe/Paris')->format('d/m/Y H:m')];
	}
}
