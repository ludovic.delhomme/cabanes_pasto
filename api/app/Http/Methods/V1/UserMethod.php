<?php 
namespace App\Http\Methods\V1;

use App\Http\Methods\BaseMethod;
use App\Models\User;

class UserMethod extends BaseMethod
{
	public function __construct(User $user)
	{
		$this->model = $user;
	}

	public function index()
	{
		return $this->model->paginate(50);
	}

	public function onlyPartners()
	{
		return $this->model->where('role_id',1)->paginate(50);
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function me($id)
	{
		$user = $this->model->findOrFail($id);
		$me = [
			"id" => $user->id,
			"firstname" => $user->firstname,
			"role" => $user->role,
			"permissions" => $user->permissions

		];
		return $me;
	}

	public function store($inputs)
	{
	    $user = new $this->model;
	    $user->fill($inputs);
			if(isset($inputs['role'])){
				$user->role_id = $inputs['role'];
			}
			$user->password = bcrypt($user->password);
	    $user->save();
	}

	public function update($inputs, $id)
	{
		$user = $this->model->find($id);
		$user->fill($inputs);
		if(isset($inputs['role'])){
			$user->role_id = $inputs['role'];
		}
		if(isset($inputs['password'])){
			$user->password = bcrypt($inputs['password']);
		}
		$user->save();
	}
    
	public function destroy($id)
	{
		$user = $this->model->find($id);
		$user->delete();
	}
}
