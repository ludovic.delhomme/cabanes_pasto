<?php 
namespace App\Http\Methods\V1;

use App\Http\Methods\BaseMethod;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;

class ImageMethod extends BaseMethod
{
	public function __construct(Image $image)
	{
		$this->model = $image;
	}

	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
		$inputs["image"]->storeAs('', $inputs["image"]->getClientOriginalName(),'images');
		$image = $this->model->firstOrNew(['name'=> $inputs["image"]->getClientOriginalName()]);
		$image->fill($inputs);
		$image->save();
	}
    
	public function destroy($id)
	{
		$image = $this->model->find($id);
		Storage::disk('images')->delete($image->name);
		$image->delete();
	}
}
