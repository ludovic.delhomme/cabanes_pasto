<?php 
namespace App\Http\Methods\V1;

use App\Http\Methods\BaseMethod;
use App\Models\Financeur;

class FinanceurMethod extends BaseMethod
{
	public function __construct(Financeur $financeur)
	{
		$this->model = $financeur;
	}

	public function index()
	{
		return $this->model->paginate(25);
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $financeur = new $this->model;
	    $financeur->fill($inputs);
	    $financeur->save();
	}

	public function update($inputs, $id)
	{
		$financeur = $this->model->find($id);
		$financeur->fill($inputs);
		$financeur->save();
	}
    
	public function destroy($id)
	{
		$financeur = $this->model->find($id);
		$financeur->delete();
	}
}
