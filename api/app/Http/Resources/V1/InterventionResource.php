<?php
namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class InterventionResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'statut' => $this->statut,
      'logement' => new LogementResource($this->whenLoaded('logement')),
      'annee' => $this->annee,
      'type' => $this->type,
      'commentaire' => $this->commentaire,
      'financements' => FinancementResource::collection($this->whenLoaded('financements')),
    ];
  }
}
