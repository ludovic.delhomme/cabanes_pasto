<?php
namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'firstname' => $this->firstname,
      'name' => $this->name,
      'email' => $this->email,
      'tel' => $this->tel,
      'organization' => $this->organization,
      'role' => new RoleResource($this->role),
      'permissions' => PermissionResource::collection($this->permissions),
    ];
  }
}
