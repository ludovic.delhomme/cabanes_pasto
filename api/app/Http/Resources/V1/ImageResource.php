<?php
namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'logement_id' => $this->logement_id,
      'name' => $this->name,
    ];
  }
}
