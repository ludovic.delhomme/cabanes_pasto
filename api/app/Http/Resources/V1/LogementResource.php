<?php
namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class LogementResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'statut' => $this->statut,
      'departement' => $this->departement,
      'commune' => $this->commune,
      'up_zp' => $this->up_zp,
      'nom' => $this->nom,
      'acces_final' => $this->acces_final,
      'temps_acces'=> $this->temps_acces,
      'proprietaire' => $this->proprietaire,
      'type' => $this->type,
      'multiusage' => $this->multiusage,
      'accueil_public' => $this->accueil_public,
      'activite_laitiere' => $this->activite_laitiere,
      'duree_utilisation' => $this->duree_utilisation,
      'etat_batiment' => $this->etat_batiment,
      'couchages_non_salaries' => $this->couchages_non_salaries,
      'salaries_max' => $this->salaries_max,
      'mixite'=> $this->mixite,
      'nb_chambres' => $this->nb_chambres,
      'surface' => $this->surface,
      'douche' => $this->douche,
      'wc' => $this->wc,
      'alim_elec' => $this->alim_elec,
      'alim_eau' => $this->alim_eau,
      'origine_eau' => $this->origine_eau,
      'qualite_eau' => $this->qualite_eau,
      'eau_potable' => $this->eau_potable,
      'dispo_eau' => $this->dispo_eau,
      'assainissement' => $this->assainissement,
      'chauffe_eau' => $this->chauffe_eau,
      'chauffage' => $this->chauffage,
      'stockage_indep' => $this->stockage_indep,
      'user' => new UserResource($this->user),
      'date_modif' => $this->updated_at->format('d/m/Y'),
      'interventions' => InterventionResource::collection($this->whenLoaded('interventions')),
      'images' => ImageResource::collection($this->whenLoaded('images')),
      'geom' => json_decode($this->geom),
    ];
  }
}
