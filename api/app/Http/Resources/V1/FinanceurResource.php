<?php
namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class FinanceurResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'nom' => $this->nom
    ];
  }
}
