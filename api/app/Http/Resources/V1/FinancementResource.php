<?php
namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class FinancementResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'intervention' => new InterventionResource($this->whenLoaded('intervention')),
      'financeur' =>new FinanceurResource($this->financeur),
      'montant' => $this->montant
    ];
  }
}
