<?php
   
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Http\Methods\V1\FinanceurMethod;
use App\Http\Resources\V1\FinanceurResource;
use App\Http\Requests\V1\Financeur\CreateRequest;
use App\Http\Requests\V1\Financeur\UpdateRequest;
   
class FinanceurController extends BaseController
{
    protected $financeurs;

    public function __construct(FinanceurMethod $financeurs)
    {
      $this->financeurs = $financeurs;
    }

    public function index()
    {
      return FinanceurResource::collection($this->financeurs->index());
    }

    public function show($id)
    {
      return new FinanceurResource($this->financeurs->find($id));
    }

    public function store(CreateRequest $request)
    {
      $this->financeurs->store($request->all());
      return $this->handleResponse('success', 'Financeur créé');
    }

    public function update(UpdateRequest $request, $id)
    {
      $this->financeurs->update($request->all(), $id);
      return $this->handleResponse('success', 'Financeur modifié');
    }

    public function destroy($id)
    {
      $this->financeurs->destroy($id);
      return $this->handleResponse('success', 'Financeur supprimé');
    }
}