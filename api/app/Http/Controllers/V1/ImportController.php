<?php 
namespace App\Http\Controllers\V1;

use App\Http\Controllers\V1\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\V1\Import\UploadRequest;
use App\Imports\DatabaseImport;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends BaseController
{
  public function store(UploadRequest $request) 
  {
    $request->file->storeAs('', 'database.xlsx');
    Excel::import(new DatabaseImport, 'database.xlsx');
    return response()->json(['status'=>'success','message'=>'Database imported']);
  }
}
