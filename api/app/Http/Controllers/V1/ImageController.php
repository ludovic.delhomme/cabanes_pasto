<?php
   
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Http\Methods\V1\ImageMethod;
use App\Http\Resources\V1\ImageResource;
use App\Http\Requests\V1\Image\CreateRequest;
use App\Http\Requests\V1\Image\UpdateRequest;
   
class ImageController extends BaseController
{
    protected $images;

    public function __construct(ImageMethod $images)
    {
      $this->images = $images;
    }

    public function index()
    {
      return ImageResource::collection($this->images->index());
    }

    public function show($id)
    {
      return new ImageResource($this->images->find($id));
    }

    public function store(CreateRequest $request)
    {
      $this->images->store($request->all());
      return $this->handleResponse('success', 'Image enregistrée');
    }

    public function update(UpdateRequest $request, $id)
    {
      $this->images->update($request->all(), $id);
      return $this->handleResponse('success', 'Image modifiée');
    }

    public function destroy($id)
    {
      $this->images->destroy($id);
      return $this->handleResponse('success', 'Image supprimée');
    }
}