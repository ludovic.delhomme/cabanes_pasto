<?php
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Exports\DatabaseExport;

class ExportController extends BaseController
{
  public function export(Request $request)
  {
    $type = $request->type;
    return (new DatabaseExport)->download('logements_pastoraux.'.$type);
  }
}