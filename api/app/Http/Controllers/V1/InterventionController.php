<?php
   
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Http\Methods\V1\InterventionMethod;
use App\Http\Methods\V1\FinancementMethod;
use App\Http\Resources\V1\InterventionResource;
use App\Http\Requests\V1\Intervention\CreateRequest;
use App\Http\Requests\V1\Intervention\UpdateRequest;
   
class InterventionController extends BaseController
{
    protected $interventions;
    protected $financements;

    public function __construct(InterventionMethod $interventions, FinancementMethod $financements)
    {
      $this->interventions = $interventions;
      $this->financements = $financements;
    }

    public function index()
    {
      return InterventionResource::collection($this->interventions->index());
    }

    public function onlyMine()
    {
      return InterventionResource::collection($this->interventions->onlyMine());
    }


    public function show($id)
    {
      return new InterventionResource($this->interventions->find($id));
    }

    public function store(CreateRequest $request)
    {
      $id = $this->interventions->store($request->except(['financements']));
      if($request['financements']){
        foreach($request['financements'] as $financement) {
          $this->financements->store([
            'intervention_id' => $id,
            'financeur_id' => $financement['financeur']['id'], 
            'montant' => $financement['montant'],
          ]);
        }
      }
      return $this->handleResponse('success', 'Intervention créée');
    }

    public function update(UpdateRequest $request, $id)
    {    
      $old = $this->interventions->find($id);  
      $oldFinancements = [];
      $keepFinancements = [];   
      foreach($old->financements as $i){
        $oldFinancements[] = $i->id;
      }
      // trouve les financements qui n'ont pas changé ou ajoute les nouveaux
      if($request->financements){
        foreach($request->financements as $financement){
          if(isset($financement->id)){
            $keepImages[] = $financement->id;
          } else {
            $this->financements->store([
              'intervention_id' => $id,
              'financeur_id' => $financement['financeur']['id'], 
              'montant' => $financement['montant'],
            ]);
          }
        }
      }
      $delFinancements = array_diff($oldFinancements,$keepFinancements);
      //Supprime les financements qui ont changé
      foreach($delFinancements as $i) {
        $this->financements->destroy($i);
      }
      $this->interventions->update($request->except(['financements']), $id);
      return $this->handleResponse('success', 'Intervention modifiée');
    }

    public function destroy($id)
    {
      $this->interventions->destroy($id);
      return $this->handleResponse('success', 'Intervention supprimée');
    }
}