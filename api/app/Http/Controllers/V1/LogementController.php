<?php
   
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Http\Methods\V1\LogementMethod;
use App\Http\Methods\V1\ImageMethod;
use App\Http\Resources\V1\LogementResource;
use App\Http\Requests\V1\Logement\CreateRequest;
use App\Http\Requests\V1\Logement\UpdateRequest;
   
class LogementController extends BaseController
{
    protected $logements;
    protected $images;

    public function __construct(LogementMethod $logements, ImageMethod $images)
    {
      $this->logements = $logements;
      $this->images = $images;
    }

    public function index()
    {
      return LogementResource::collection($this->logements->index());
    }

    public function public()
    {
      return $this->logements->public();
    }

    public function onlyMine()
    {
      return LogementResource::collection($this->logements->onlyMine());
    }

    public function geojson()
    {
      return $this->handleResponse($this->logements->geojson(), 'success');
    }

    public function geojson_public()
    {
      return $this->handleResponse($this->logements->geojson_public(), 'success');
    }

    public function qgis()
    {
      return $this->logements->geojson();
    }

    public function last()
	{
		return $this->handleResponse($this->logements->last(), 'dernière mise à jour des logements');
	}

    public function show($id)
    {
      return new LogementResource($this->logements->find($id));
    }

    public function store(CreateRequest $request)
    {
      $geom = json_decode($request->geom);
      $id = $this->logements->get_id($geom->dep);
      $request->merge([
        'id' => $id,
        
        'geom' => $this->logements->geojson_to_geom($geom->geom),
        'up_zp' => $geom->up,
        'commune' => $geom->com,
        'departement' => $geom->dep,
      ]);
      $this->logements->store($request->except(['images']));
      if($request->file('images')){
        foreach($request->file('images') as $image) {
          $this->images->store(['image' => $image, 'logement_id' => $id,]);
        }
      }
      return $this->handleResponse('success', 'Logement créé');
    }

    public function update(UpdateRequest $request, $id)
    {
      $old = $this->logements->find($id);  
      $oldImages = [];
      $keepImages = [];   
      foreach($old->images as $i){
        $oldImages[] = $i->id;
      }
      // trouve les images qui n'ont pas changé ou ajoute les nouvelles images
      if($request->keep_img){
        foreach($request->keep_img as $image){
          $keepImages[] = json_decode($image)->id;
        }
      }
      if($request->images){
        foreach($request->images as $image){    
          $this->images->store(['image' => $image, 'logement_id' => $id]);
        }
      }
      $delImages = array_diff($oldImages,$keepImages);
      //Supprime les images qui ont changé
      foreach($delImages as $i) {
        $this->images->destroy($i);
      }
      // update other fields
      $geom = json_decode($request->geom);
      if(isset($geom->geom)){
        $request->merge([       
          'geom' => $this->logements->geojson_to_geom($geom->geom),
          'up_zp' => $geom->up,
          'commune' => $geom->com,
          'departement' => $geom->dep,
        ]);
      } else {
        $request->merge([
          'geom' => $this->logements->geojson_to_geom($geom)
        ]);
      }
      $this->logements->update($request->except(['images']), $id);
      return $this->handleResponse('success', 'Logement modifié');
    }

    public function destroy($id)
    {
      $this->logements->destroy($id);
      return $this->handleResponse('success', 'Logement supprimé');
    }
    
    public function search(Request $request)
    {
      return $this->logements->search($request['text'],$request['auth']);
    }

}