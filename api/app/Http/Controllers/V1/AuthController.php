<?php
   
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Http\Resources\V1\UserResource;
use Laravel\Sanctum\HasApiTokens;
use App\Models\User;
use Validator;

use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends BaseController
{
  
  public function login(Request $request)
  {
    if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
      $auth = Auth::user(); 
      $success['token'] =  $auth->createToken('LaravelSanctumAuth')->plainTextToken; 
      $success['user'] =  ['firstname'=>$auth->firstname,'name'=>$auth->name];

      return $this->handleResponse($success, 'User logged-in!');
    } 
    else
    { 
      return $this->handleError('Unauthorised.', ['error'=>'Unauthorised']);
    } 
  }

  public function logout()
  {
    if(auth()->user())
    {
      auth()->user()->tokens()->delete();
      return $this->handleResponse('success','User logged-out!');
    }
    else
    { 
      return $this->handleError('Unauthenticated.', ['error'=>'Unauthenticated'],401);
    } 
  }

  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required',
      'email' => 'required|email',
      'password' => 'required',
      'confirm_password' => 'required|same:password',
    ]);

    if($validator->fails()){
      return $this->handleError($validator->errors());       
    }

    $input = $request->all();
    $input['password'] = bcrypt($input['password']);
    $user = User::create($input);
    $success['token'] =  $user->createToken('LaravelSanctumAuth')->plainTextToken;
    $success['name'] =  $user->name;

    return $this->handleResponse($success, 'User successfully registered!');
  }
  
  public function me()
  {
    return new UserResource(Auth::user());
  }
}