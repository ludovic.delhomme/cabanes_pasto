<?php
   
namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\BaseController as BaseController;
use App\Http\Methods\V1\FinancementMethod;
use App\Http\Resources\V1\FinancementResource;
use App\Http\Requests\V1\Financement\CreateRequest;
use App\Http\Requests\V1\Financement\UpdateRequest;
   
class FinancementController extends BaseController
{
    protected $financements;

    public function __construct(FinancementMethod $financements)
    {
      $this->financements = $financements;
    }

    public function index()
    {
      return FinancementResource::collection($this->financements->index());
    }

    public function show($id)
    {
      return new FinancementResource($this->financements->find($id));
    }

    public function store(CreateRequest $request)
    {
      $this->financements->store($request->all());
      return $this->handleResponse('success', 'Financement crée');
    }

    public function update(UpdateRequest $request, $id)
    {
      $this->financements->update($request->all(), $id);
      return $this->handleResponse('success', 'Financement modifié');
    }

    public function destroy($id)
    {
      $this->financements->destroy($id);
      return $this->handleResponse('success', 'Financement supprimé');
    }
}