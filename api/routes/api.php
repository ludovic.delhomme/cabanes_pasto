<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\AuthController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\V1\ImportController;
use App\Http\Controllers\V1\ExportController;
use App\Http\Controllers\V1\UserController;
use App\Http\Controllers\V1\RoleController;
use App\Http\Controllers\V1\PermissionController;
use App\Http\Controllers\V1\LogementController;
use App\Http\Controllers\V1\FinanceurController;
use App\Http\Controllers\V1\InterventionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('/password/email', [PasswordResetLinkController::class, 'store']);
    Route::post('/password/reset', [NewPasswordController::class, 'store'])->name('password.reset');
    Route::get('logements/public', [LogementController::class, 'public']);
    Route::get('logements/qgis', [LogementController::class, 'qgis']);
    Route::get('logements/update', [LogementController::class, 'last']);
    Route::get('logements/geojson-public', [LogementController::class, 'geojson_public']);
    Route::post('logements/search', [LogementController::class,'search']);
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('me', [AuthController::class, 'me']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('import', [ImportController::class, 'store']);
        Route::post('export', [ExportController::class, 'export']);
        Route::get('logements/me', [LogementController::class, 'onlyMine']);
        Route::get('logements/geojson', [LogementController::class, 'geojson']);
        Route::post('logements/{id}',[LogementController::class,'update']);
        Route::resource('logements', LogementController::class);
        Route::resource('financeurs', FinanceurController::class);
        Route::get('interventions/me', [InterventionController::class, 'onlyMine']);
        Route::resource('interventions', InterventionController::class);
        Route::middleware(['can:manage_users'])->group(function () {
            Route::get('users/partners', [UserController::class, 'onlyPartners']);
            Route::resource('users', UserController::class);
        });
        Route::middleware(['can:manage_backend'])->group(function () {
            Route::resource('roles', RoleController::class);
            Route::resource('permissions', PermissionController::class);
        });
    });
});
