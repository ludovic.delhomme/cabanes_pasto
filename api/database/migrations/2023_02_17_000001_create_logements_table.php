<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logements', function (Blueprint $table) {
            $table->string('id',6)->unique()->index();
            $table->string('statut')->default('existant');
            $table->string('departement',2);
            $table->string('commune',5);
            $table->string('up_zp',9)->nullable();
            $table->string('nom')->nullable();
            $table->string('acces_final')->nullable();
            $table->integer('temps_acces')->nullable();
            $table->string('proprietaire')->nullable();
            $table->string('type')->nullable();
            $table->string('multiusage')->default('uniquement pastoral');
            $table->string('accueil_public')->default('aucun accueil du public');
            $table->string('activite_laitiere')->nullable();
            $table->integer('duree_utilisation')->nullable();
            $table->string('etat_batiment')->nullable();
            $table->integer('couchages_non_salaries')->nullable();
            $table->integer('salaries_max')->nullable();
            $table->boolean('mixite')->nullable();
            $table->integer('nb_chambres')->nullable();
            $table->string('surface')->nullable();
            $table->boolean('douche')->nullable();
            $table->string('wc')->nullable();
            $table->string('alim_elec')->nullable();
            $table->string('alim_eau')->nullable();
            $table->string('origine_eau')->nullable();
            $table->string('qualite_eau')->nullable();
            $table->string('dispo_eau')->nullable();
            $table->string('assainissement')->nullable();
            $table->string('chauffe_eau')->nullable();
            $table->boolean('chauffage')->nullable();
            $table->boolean('stockage_indep')->nullable();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->point('geom')->nullable()->spatialIndex();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logements');
    }
};
