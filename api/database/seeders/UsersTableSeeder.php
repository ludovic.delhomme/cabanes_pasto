<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Model::unguard();
      DB::table('users')->delete();
      User::create([
        'id' => 1,
        'firstname' => 'Ludovic',
        'name' => 'Delhomme',
        'email' => 'ludovic.delhomme@datayama.fr',
        'tel' => '06 24 58 23 92',
        'organization' => 'datayama', 
        'password' => bcrypt('admin'),
        'role_id' => 4,
      ]);
      User::create([
        'id' => 2,
        'firstname' => 'Frédéric',
        'name' => 'Bray',
        'email' => 'frederic.bray@inrae.fr',
        'tel' => '06 16 86 16 63',
        'organization' => 'INRAe', 
        'password' => bcrypt('admin'),
        'role_id' => 4,
      ]);
      User::create([
        'id' => 3,
        'firstname' => 'Olivier',
        'name' => 'Bonnet',
        'email' => 'obonnet@cerpam.fr',
        'tel' => '',
        'organization' => 'CERPAM', 
        'password' => bcrypt('admin'),
        'role_id' => 4,
      ]);
      User::create([
        'id' => 11,
        'firstname' => 'SEA74',
        'name' => 'Haute-Savoie',
        'email' => 'accueil@sea74.fr',
        'tel' => '',
        'organization' => 'SEA74', 
        'password' => bcrypt('admin'),
        'role_id' => 2,
      ]);
      User::create([
        'id' => 12,
        'firstname' => 'FAI',
        'name' => 'Isère',
        'email' => 'federation@alpages38.org',
        'tel' => '',
        'organization' => 'FAI', 
        'password' => bcrypt('admin'),
        'role_id' => 2,
      ]);
      User::create([
        'id' => 13,
        'firstname' => 'Clément',
        'name' => 'Teppaz',
        'email' => 'cteppaz@sea73.fr',
        'tel' => '',
        'organization' => 'SEA 73', 
        'password' => bcrypt('admin'),
        'role_id' => 2,
      ]);
      User::create([
        'id' => 14,
        'firstname' => 'Violette',
        'name' => 'REYSSET',
        'email' => 'accueil@adem-drome.fr',
        'tel' => '',
        'organization' => 'ADEM', 
        'password' => bcrypt('admin'),
        'role_id' => 2,
      ]);
      User::create([
        'id' => 15,
        'firstname' => 'Brice',
        'name' => 'Thollet',
        'email' => 'brice.thollet@agriculture.gouv.fr',
        'tel' => '',
        'organization' => 'DRAFF', 
        'password' => bcrypt('admin'),
        'role_id' => 1,
      ]);
      User::create([
        'id' => 16,
        'firstname' => 'Cerpam',
        'name' => 'Paca',
        'email' => 'secretariat@cerpam.fr',
        'tel' => '',
        'organization' => 'CERPAM', 
        'password' => bcrypt('admin'),
        'role_id' => 2,
      ]);
      User::create([
        'id' => 18,
        'firstname' => 'Théa',
        'name' => 'Broccolichi',
        'email' => 'tbroccolichi@cerpam.fr',
        'tel' => '',
        'organization' => 'CERPAM', 
        'password' => bcrypt('admin'),
        'role_id' => 2,
      ]);
      Model::reguard();
    }
}
