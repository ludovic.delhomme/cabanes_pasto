<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('permissions')->delete();
        Permission::create([
            'slug' => 'manage_backend',
            'name' => 'Gestion du backend',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Permission::create([
            'slug' => 'manage_users',
            'name' => 'Gestion des utilisateurs partenaires',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Permission::create([
            'slug' => 'manage_all_users',
            'name' => 'Gestion de tous les utilisateurs',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Permission::create([
            'slug' => 'manage_all_logements',
            'name' => 'Gestion de tous les logements',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Permission::create([
            'slug' => 'manage_logements',
            'name' => 'Gestion de mes logements',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Permission::create([
            'slug' => 'manage_financeurs',
            'name' => 'Gestion des financeurs',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Permission::create([
            'slug' => 'manage_download',
            'name' => 'Possibilité d\'exporter la base de donnée',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        Model::reguard();
    }
}
